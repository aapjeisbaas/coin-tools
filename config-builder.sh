#!/bin/bash

# get markets
MARKETS=$(./market-finder.sh | head -n 5 | sort -n -k 2 -r | awk '{print $1}')

i=1
for PAIR in $MARKETS; do
  echo "$PAIR" > worker-conf/$i
  i=$((i+1))
done
