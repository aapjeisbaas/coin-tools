import datetime as dt
import matplotlib.pyplot as plt
from matplotlib import style
from matplotlib.finance import candlestick_ohlc
import matplotlib.dates as mdates
import pandas as pd
import pandas_datareader.data as web
import numpy as np
import time
import json
from datetime import timedelta
import arrow
import logging
import requests
from pandas.io.json import json_normalize
from stockstats import StockDataFrame


style.use('ggplot')

#df = pd.read_csv('tsla.csv', parse_dates=True, index_col=0)

#df['100ma'] = df['Close'].rolling(window=100, min_periods=0).mean()




def get_ticker_dataframe(pair):
    """
    Analyses the trend for the given pair
    :param pair: pair as str in format BTC_ETH or BTC-ETH
    :return: StockDataFrame
    """
    minimum_date = arrow.now() - timedelta(days=4)

    url = 'https://bittrex.com/Api/v2.0/pub/market/GetTicks'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
    }
    """
    tickInterval:
    OneMin
    FiveMin
    Hour
    Day
    """
    params = {
        'marketName': pair.replace('_', '-'),
        'tickInterval': 'FiveMin',
        '_': minimum_date.timestamp * 1000
    }
    data = requests.get(url, params=params, headers=headers).json()
    if not data['success']:
        raise RuntimeError('BITTREX: {}'.format(data['message']))

    data = [{
        'close': t['C'],
        'volume': t['V'],
        'open': t['O'],
        'high': t['H'],
        'low': t['L'],
        'date': t['T'],
    } for t in sorted(data['result'], key=lambda k: k['T']) if arrow.get(t['T']) > minimum_date]
    dataframe = StockDataFrame(json_normalize(data))
    
    # date:
    # 2017-08-26T04:10:00
    dataframe['datetime'] = pd.to_datetime(dataframe['date'], format='%Y-%m-%dT%H:%M:%S')
    
    dataframe.reset_index()
    dataframe = dataframe.set_index('datetime')

    """
    # calculate StochRSI
    window = 14
    rsi = dataframe['rsi_{}'.format(window)]
    rolling = rsi.rolling(window=window, center=False)
    low = rolling.min()
    high = rolling.max()
    dataframe['stochrsi'] = (rsi - low) / (high - low)
    """

    
    dataframe['vol_short_ma'] = dataframe['volume'].rolling(window=3, min_periods=0).mean()
    dataframe['vol_long_ma'] = dataframe['volume'].rolling(window=21, min_periods=0).mean()
    #dataframe['21ma'] = dataframe['Close'].rolling(window=21, min_periods=0).mean()
    #dataframe['10ma'] = dataframe['Close'].rolling(window=10, min_periods=0).mean()
    dataframe['long_ma'] = dataframe['close'].ewm(span=25,adjust=False).mean()
    dataframe['short_ma'] = dataframe['close'].ewm(span=7,adjust=False).mean()
    return dataframe

def analyze_long(dataframe):

    #dataframe.set_index('date')
    #dataframe.reset_index().set_index('date').resample('5M').mean()
    #dataframe.resample('30S').asfreq()[0:5] 
    dataframelong = dataframe.resample('H').mean()
    dataframelong['long_long_ma'] = dataframelong['close'].ewm(span=25,adjust=False).mean()
    dataframelong['long_short_ma'] = dataframelong['close'].ewm(span=7,adjust=False).mean()

    
    dataframelong.loc[
        (dataframelong['long_short_ma'] > dataframelong['long_long_ma']),
        'ema'
    ] = 1

    dataframelong.loc[
        (dataframelong['long_short_ma'] < dataframelong['long_long_ma']),
        'ema'
    ] = 0

    # Buy
    dataframelong.loc[
        (dataframelong['ema'] == 1),
        'action_long'
    ] = 1
        # Sell
    dataframelong.loc[
         (dataframelong['ema'] == 0),
        'action'
    ] = -1

    return dataframelong


def analyze(dataframe):
    dataframe.loc[
        (dataframe['short_ma'] > dataframe['long_ma']),
        'ema'
    ] = 1

    dataframe.loc[
        (dataframe['short_ma'] < dataframe['long_ma']),
        'ema'
    ] = 0

    # Buy
    dataframe.loc[
        (dataframe['ema'] == 1) &
        (dataframe['adx'] < 22),
        'action'
    ] = 1


    # Hodl don't double buy
    dataframe.loc[
        (dataframe['action'] == 1) &
        (dataframe.shift(periods=1, axis=0)['action'] == 1),
        'action'
    ] = 0


    # Sell
    dataframe.loc[
        (dataframe['adx'] > 40) &
        (dataframe['macd'] > 0),
        'action'
    ] = -1
    
    # Sell
    dataframe.loc[
        (dataframe['macds'] > dataframe['macd']) |
         (dataframe['ema'] == 0),
        'action'
    ] = -1

    # Hodl
    dataframe.loc[
        (dataframe['action'] != 1) &
        (dataframe['action'] != -1),
        'action'
    ] = 0


    return dataframe


def get_pairs():
    minimum_volume=500.0
    url = 'https://bittrex.com/api/v1.1/public/getmarketsummaries'
    data = requests.get(url).json()
    if not data['success']:
        raise RuntimeError('BITTREX: {}'.format(data['message']))

    data = [{
        'MarketName': t['MarketName'],
        'High': t['High'],
        "Low": t['Low'],
        "Volume": t['Volume'],
        "Last": t['Last'],
        "BaseVolume": t['BaseVolume'],
        "TimeStamp": t['TimeStamp'],
        "Bid": t['Bid'],
        "Ask": t['Ask'],
        "OpenBuyOrders": t['OpenBuyOrders'],
        "OpenSellOrders": t['OpenSellOrders'],
        "PrevDay": t['PrevDay'],
        "Created": t['Created'],
    } for t in sorted(data['result'], key=lambda k: k['Volume']) if t['BaseVolume'] > minimum_volume and "BTC-" in t['MarketName'] and t['Last'] > t['PrevDay'] and t['Last'] > 0.0001 ]
    dataframe = StockDataFrame(json_normalize(data))
    return dataframe

    
pairsDf = get_pairs()
pairs = pairsDf['MarketName'].tolist()

#pairs = ['BTC_XRP', 'BTC_BCC', 'BTC_OMG', 'BTC_ERC', 'BTC_MCO']

for pair in pairs:
    dataframe = get_ticker_dataframe(pair)
    dataframe = analyze(dataframe)
    dataframe.to_csv(pair+".csv")
    dataframelong = analyze_long(dataframe)
    dataframelong.to_csv(pair+".long.csv")
    dataframelong.reset_index()

    
    actionlong = dataframelong['action_long'][dataframelong.index[-1]]
    #actionlong = dataframelong.tail(1)['action_long']
    action = dataframe['action'][dataframe.index[-1]]

    #print(actionlong)
    
    signal = ( actionlong + action )
    print("Pair:", pair, "\tAction:", action, "\tLong:", actionlong, "Signal: %d" % (signal))
    #print("Pair:", pair, "Action:", action, "Long", actionlong)
    #if signal > buy:
    #    print("BUY:", pair)

    

"""
df_ohlc = df['Close'].resample('10D').ohlc()
df_volume = df['Volume'].resample('10D').sum()
df_ohlc.reset_index(inplace=True)
df_ohlc['Date'] = df_ohlc['Date'].map(mdates.date2num)
print(df.tail())
ax1 = plt.subplot2grid((6,1), (0,0), rowspan=5, colspan=1)
ax2 = plt.subplot2grid((6,1), (5,0), rowspan=1, colspan=1, sharex=ax1)
ax1.xaxis_date()

candlestick_ohlc (ax1, df_ohlc.values, width=2, colorup='g')
ax2.fill_between(df_volume.index.map(mdates.date2num), df_volume.values, 0)

plt.show()
"""
