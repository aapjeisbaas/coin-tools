pairs=$(find *.csv -newermt "-1 hours" -ls)


head -n1 BTC-BCC.csv | sed 's/,/\n/g' | grep -B9999 action |wc -l



ROWS=$(cat *.csv | head -n 1000 | awk -F"," '{print $2","$3 "," $36 }')

declare -a buy
declare -a hodl
declare -a sell
declare -a profit
i=0
for row in $ROWS ; do
  i=$((i+1))
#  echo $i
  buy=$(echo $row |grep ":00,1.0")
  hodl=$(echo $row |grep ":00,0")
  sell=$(echo $row |grep ":00,\-1.0")

  if [ "${#buy}" -gt "5" ]; then
#    echo "BUY"
    buy[$i]="$row"
  fi
  if [ "${#hodl}" -gt "5" ]; then
#    echo "hodl"
    hodl[$i]="$row"
  fi
  if [ "${#sell}" -gt "5" ]; then
#    echo "SELL"
    sell[$i]="$row"
  fi
done

echo "Loops: $i"

p=1
r=0
while [ "$i" -gt "$r" ]; do

#  echo $r

  # find buyline
  buy_line=$(echo ${buy[$r]})
  if [ "${#buy_line}" -gt "5" ]; then
    # find first sell line after that
    t=$r
    found="false"
    while [ "$found" != "true" ]; do
      t=$((t+1))
      sell_line=$(echo ${sell[$t]})
#      echo $t
      if [ "${#sell_line}" -gt "5" ]; then
        a=$(echo ${buy[$((t-1))]})
        b=$(echo ${hodl[$((t-1))]})
        c=$(echo ${sell[$((t-1))]})
        sell_line="$a$b$c"
        found="true"
      fi
    done
    buy_price=$(echo "$buy_line" | awk -F"," '{print $1}')
    sell_price=$(echo "$sell_line" | awk -F"," '{print $1}')
    profit_percent=$(python -c "print ((($sell_price/$buy_price)*100)-100.5) " )
    profit[$p]="$profit_percent"
    p=$((p+1))
  fi
  r=$((r+1))
done


PROFIT_OUTPUT=$(echo "${profit[*]}" |sed 's/\ /, /g')
SUM_CMD="print(sum([$PROFIT_OUTPUT]))"
TOT_PROFIT=$(python -c "${SUM_CMD}")

echo "$TOT_PROFIT"

