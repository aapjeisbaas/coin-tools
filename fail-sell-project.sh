PAIR="BTC-CVC" 

while true; do 
  echo "60: $HIGH_60"
  echo "30: $HIGH_30"
  echo "05: $HIGH_05"
  echo "01: $HIGH_01"
  echo "00: $HIGH_00"

  echo "Direction: $direction"

  NOW=$(date +%s)
  

  HIGH_60=$(curl -s "https://bittrex.com/Api/v2.0/pub/market/GetLatestTick?marketName=$PAIR&tickInterval=hour&_=$NOW" | jq -r '.result[] .H')
  HIGH_30=$(curl -s "https://bittrex.com/Api/v2.0/pub/market/GetLatestTick?marketName=$PAIR&tickInterval=thirtyMin&_=$NOW" | jq -r '.result[] .H')
  HIGH_05=$(curl -s "https://bittrex.com/Api/v2.0/pub/market/GetLatestTick?marketName=$PAIR&tickInterval=fiveMin&_=$NOW" | jq -r '.result[] .H')
  HIGH_01=$(curl -s "https://bittrex.com/Api/v2.0/pub/market/GetLatestTick?marketName=$PAIR&tickInterval=oneMin&_=$NOW" | jq -r '.result[] .H')
  HIGH_00=$(curl -s "https://bittrex.com/api/v1.1/public/getticker?market=$PAIR"|jq '.result.Last')

  # get direction
  HIGH_60_30=$(python -c "print $HIGH_30-$HIGH_60")
  HIGH_30_05=$(python -c "print $HIGH_05-$HIGH_30")
  HIGH_05_01=$(python -c "print $HIGH_01-$HIGH_05")
  HIGH_01_00=$(python -c "print $HIGH_00-$HIGH_01")

  UP=false
  
  HIGH_60_30_DIR=$(if [ "$(python -c "if ($HIGH_60_30 > 0): print 'UP'")" = "UP" ]; then echo "^"; UP=true ; else echo "."; fi)
  HIGH_30_05_DIR=$(if [ "$(python -c "if ($HIGH_30_05 > 0): print 'UP'")" = "UP" ]; then echo "^"; UP=true ; else echo "."; fi)
  HIGH_05_01_DIR=$(if [ "$(python -c "if ($HIGH_05_01 > 0): print 'UP'")" = "UP" ]; then echo "^"; UP=true ; else echo "."; fi)
  HIGH_01_00_DIR=$(if [ "$(python -c "if ($HIGH_01_00 > 0): print 'UP'")" = "UP" ]; then echo "^"; UP=true ; else echo "."; fi)

  direction="$HIGH_60_30_DIR $HIGH_30_05_DIR $HIGH_05_01_DIR $HIGH_01_00_DIR"

  if $UP ;then
    echo "adjusting stop"
    STOP=$(python -c "print $HIGH_05 / 1.03")

  fi

  sleep 1
  clear
done
