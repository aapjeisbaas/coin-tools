#!/bin/bash

btc_eur=$(curl -s 'https://api.coinmarketcap.com/v1/ticker/bitcoin/?convert=EUR' | jq -r '.[] .price_eur')
eth_eur=$(curl -s 'https://api.coinmarketcap.com/v1/ticker/ethereum/?convert=EUR' | jq -r '.[] .price_eur')
zec_eur=$(curl -s 'https://api.coinmarketcap.com/v1/ticker/zcash/?convert=EUR' | jq -r '.[] .price_eur')

echo "btc: $btc_eur eth: $eth_eur zec: $zec_eur"
