# fetch api keys:
source api_keys.sh

# hash_hmac function
function hash_hmac {
  digest="$1"
  data="$2"
  key="$3"
  shift 3
  echo -n "$data" | openssl dgst "-$digest" -hmac "$key" "$@"
}

# nonce returning function
function nonce {
  cat /dev/urandom | tr -dc '0-9' | fold -w 256 | head -n 1 | sed -e 's/^0*//' | head --bytes 16
}

function buy {
  JSON=""
  UUID=""
  STATUS=""
  MESSAGE=""

  pair="$1"
  quantity="$2"
  rate="$3"

  NONCE=$(nonce)
  URI="https://bittrex.com/api/v1.1/market/buylimit?apikey=${API_KEY}&market=${pair}&quantity=${quantity}&rate=${rate}&nonce=${NONCE}"
  SIGN=$(hash_hmac "sha512" $URI $API_SECRET | awk '{print $2}')
  JSON=$(curl -s -H "apisign: $SIGN" "$URI")
  UUID=$(echo $JSON | jq -r '.result .uuid')
  
  STATUS=$(echo $JSON | jq -r '.success')
  MESSAGE=$(echo $JSON | jq -r '.message')

  if [ "$STATUS" = "false" ] ; then
    RETURN=2
  else
    RETURN=0
  fi
  return $RETURN
}

function check_order {
  JSON=""
  UUID=""
  STATUS=""
  MESSAGE=""

  UUID="$1"

  if [ "$DEBUG" = "YES"  ]; then
   echo "cheching UUID: $UUID"
  fi

  NONCE=$(nonce)
  URI="https://bittrex.com/api/v1.1/account/getorder?apikey=${API_KEY}&uuid=${UUID}&nonce=${NONCE}"
  SIGN=$(hash_hmac "sha512" $URI $API_SECRET | awk '{print $2}')
  JSON=$(curl -s -H "apisign: $SIGN" "$URI")
  RESULT=$(echo $JSON | jq -r '.result')

  STATUS=$(echo $JSON | jq -r '.success')
  MESSAGE=$(echo $JSON | jq -r '.message')
  CLOSED=$(echo $RESULT |  jq '. | select (.Closed!=null)')

  if [ "$DEBUG" = "YES"  ]; then
    echo "Status:  $STATUS "
    echo "Message: $MESSAGE"
    echo "Closed:  $CLOSED "
  fi

  if [ ${#CLOSED} -gt 2 ]; then
    echo "Closed the buy order."
    RETURN=0
  else
    RETURN=1
  fi
  return $RETURN
  
}

function sell_order {
  JSON=""
  UUID=""
  STATUS=""
  MESSAGE=""

  pair="$1"
  quantity="$2"
  rate="$3"
 
  # create sell order
  NONCE=$(nonce)
  URI="https://bittrex.com/api/v1.1/market/selllimit?apikey=${API_KEY}&market=${pair}&quantity=${quantity}&rate=${rate}&nonce=${NONCE}"
  SIGN=$(hash_hmac "sha512" $URI $API_SECRET | awk '{print $2}')
  JSON=$(curl -s -H "apisign: $SIGN" "$URI")

  UUID=$(echo $JSON | jq -r '.result .uuid')

  if [ "$DEBUG" = "YES"  ]; then
    echo $JSON | jq 
  fi
}

function stop_order {
  uuid="$1"

  echo "stopping order: $uuid"

  NONCE=$(nonce)
  URI="https://bittrex.com/api/v1.1/market/cancel?apikey=${API_KEY}&uuid=${uuid}&nonce=${NONCE}"
  SIGN=$(hash_hmac "sha512" $URI $API_SECRET | awk '{print $2}')
  JSON=$(curl -s -H "apisign: $SIGN" "$URI")

  if [ "$DEBUG" = "YES"  ]; then
    echo $JSON | jq -r
  fi

  echo "actions stopped"
}
