#!/bin/bash


# parse input
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -p|--pair)
    PAIR="$2"
    shift # past argument
    ;;
    -m|--min)
    MAX_BUY="$2"
    shift # past argument
    ;;
    --help)
    HELP=true
    ;;
    --default)
    DEFAULT=YES
    ;;
    *)
    # unknown option
    HELP=true
    ;;
esac
shift # past argument or value
done

if [ "$HELP" = true ] ; then
  echo "HELP"
  exit 2
fi

# fetch api keys:
source api_keys.sh

# split the pair in to and from
TO=$(echo $PAIR | sed 's/-.*//g')
FROM=$(echo $PAIR | sed 's/.*-//g')


# hash_hmac function
function hash_hmac {
  digest="$1"
  data="$2"
  key="$3"
  shift 3
  echo -n "$data" | openssl dgst "-$digest" -hmac "$key" "$@"
}

# nonce returning function
function nonce {
  cat /dev/urandom | tr -dc '0-9' | fold -w 256 | head -n 1 | sed -e 's/^0*//' | head --bytes 16
}


# get market
MARKET=$(curl -s "https://bittrex.com/api/v1.1/public/getmarketsummary?market=$PAIR" | jq -r '.result')

# get current balance
NONCE=$(nonce)
URI="https://bittrex.com/api/v1.1/account/getbalances?apikey=${API_KEY}&nonce=${NONCE}"
SIGN=$(hash_hmac "sha512" $URI $API_SECRET | awk '{print $2}')
JSON=$(curl -s -H "apisign: $SIGN" "$URI")
TO_AVAIL=$(echo $JSON | jq -r '.result[] |select (.Currency=="'$FROM'") .Available')
FROM_AVAIL=$(echo $JSON | jq -r '.result[] |select (.Currency=="'$TO'") .Available')

# get pending sell 
PERCENT_INV=$(python -c "print $PERCENT_INV/100.0")
INV_TOTAL=$(python -c "print $PERCENT_INV*$FROM_AVAIL")
LAST_LOW=$(curl -s "https://bittrex.com/api/v1.1/public/getmarkethistory?market=$PAIR" |jq -r '.result [] .Price' |sort -nr |tail -n 1)

LOW=$LAST_LOW
HIGH=$LAST

echo $LOW
echo $HIGH


# setup buy style 
case "$BUYSTYLE" in 
  
  1 ) 
  BUY=$LAST
  ;;
  
  [2-9] )
  BUY=$(python -c "print (((5-$BUYSTYLE)*$LOW)+((5+$BUYSTYLE)*$HIGH))/10")
  ;;

  10 )
  BUY=$LOW
  ;; 
  
esac

if (( $(echo "$BUY > $MAX_BUY" |bc -l) )); then
  echo "price above limit"
  echo $BUY
  exit 0
fi

# set sell amount
PERCENT_RET=$(echo "$PERCENT_RET + 100" | bc)
INT_RATE=$(python -c "print $PERCENT_RET/100.0")
MIN_SELL=$(python -c "print $BUY*$INT_RATE")

# market info
PAIR_SUM=$(echo $MARKET |  jq -r '.[] |select (.MarketName=="'$PAIR'")')
DAY_HIGH=$(echo $PAIR_SUM | jq -r '.High') 

echo "Balances:"
echo "Invest:  $INV_TOTAL"
echo "$FROM:     $FROM_AVAIL"
echo   "$TO:     $TO_AVAIL"



echo "Ticker:"
echo $TICKER | jq

echo "200 Low:  $LAST_LOW"
echo "Day High: $DAY_HIGH"
echo "Buy:      $BUY" 
echo "Sell:     $MIN_SELL"


# buy order

INV_TO=$(python -c "print $INV_TOTAL/$BUY")
echo $INV_TO

NONCE=$(nonce)
URI="https://bittrex.com/api/v1.1/market/buylimit?apikey=${API_KEY}&market=${PAIR}&quantity=${INV_TO}&rate=${BUY}&nonce=${NONCE}"
SIGN=$(hash_hmac "sha512" $URI $API_SECRET | awk '{print $2}')
JSON=$(curl -s -H "apisign: $SIGN" "$URI")
echo $JSON | jq 

UUID=$(echo $JSON | jq -r '.result .uuid')
echo $UUID

#if json .success ....
waiting=1


echo "waiting for buy to pass"
spin='-\|/'
spini=0

while [ $waiting -eq 1 ]; do
  # prety wait spinning thing
  i=$(( (i+1) %4 ))
  printf "\r${spin:$i:1}"

  NONCE=$(nonce)
  URI="https://bittrex.com/api/v1.1/account/getorder?apikey=${API_KEY}&uuid=${UUID}&nonce=${NONCE}"
  SIGN=$(hash_hmac "sha512" $URI $API_SECRET | awk '{print $2}')
  JSON=$(curl -s -H "apisign: $SIGN" "$URI")
  RESULT=$(echo $JSON | jq -r '.result')

  CLOSED=$(echo $RESULT |  jq '. | select (.Closed!=null)')
  if [ ${#CLOSED} -gt 2 ]; then
    echo "closed the buy order"
    waiting=0
  fi

  sleep .2
done


echo ""
echo "Sell at: $MIN_SELL"

# create sell order
NONCE=$(nonce)
URI="https://bittrex.com/api/v1.1/market/selllimit?apikey=${API_KEY}&market=${PAIR}&quantity=${INV_TO}&rate=${MIN_SELL}&nonce=${NONCE}"
SIGN=$(hash_hmac "sha512" $URI $API_SECRET | awk '{print $2}')
JSON=$(curl -s -H "apisign: $SIGN" "$URI")
echo $JSON | jq 

UUID=$(echo $JSON | jq -r '.result .uuid')
echo $UUID
