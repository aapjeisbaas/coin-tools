#!/bin/bash

MAX_OPEN=5
MAX_OPEN_TOTAL=20
MAX_BUY=1
DELAY=0
LOOPS=400


# parse input
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -p|--pair)
    PAIR="$2"
    shift # past argument
    ;;
    -r|--roi)
    PERCENT_RET="$2"
    shift # past argument
    ;;
    -i|--investpercent)
    PERCENT_INV="$2"
    shift # past argument
    ;;
    -b|--buystyle)
    BUYSTYLE="$2"
    shift # past argument
    ;;
    -m|--max)
    MAX_BUY="$2"
    shift # past argument
    ;;
    -t|--tradesopen)
    MAX_OPEN="$2"
    shift # past argument
    ;;
    -d|--delay)
    DELAY="$2"
    shift # past argument
    ;;
    -l|--loops)
    LOOPS="$2"
    shift # past argument
    ;;
    -c|--creep)
    CREEP_NUM="$2"
    CREEP="true"
    shift # past argument
    ;;
    --diffcheck)
    DIFF_CHECK="true"
    ;;
    --help|-h|help)
    HELP="true"
    ;;
    --debug)
    DEBUG="YES"
    ;;
    *)
    # unknown option
    HELP="true"
    ;;
esac
shift # past argument or value
done

if [[ $# -eq 1 ]]; then
  HELP="true"
fi
if [ "$HELP" = "true" ] ; then
  echo "HELP"
  exit 2
fi

# import bittrex functions
source bittrex-functions.sh

# split the pair in to and from
FROM=$(echo $PAIR | sed 's/-.*//g')
TO=$(echo $PAIR | sed 's/.*-//g')

# add the transaction fees to the roi var
PERCENT_RET=$(python -c "print $PERCENT_RET+0.5")

# get current pending orders to check if we should buy more
NONCE=$(nonce)
URI="https://bittrex.com/api/v1.1/market/getopenorders?apikey=${API_KEY}&market=${PAIR}&nonce=${NONCE}"
SIGN=$(hash_hmac "sha512" $URI $API_SECRET | awk '{print $2}')
JSON=$(curl -s -H "apisign: $SIGN" "$URI")

OPEN_ORDERS=$(echo $JSON | jq -r '[.result[] |select (.Closed == null) .OrderUuid] | length')

if [ "$OPEN_ORDERS" -ge "$MAX_OPEN" ]; then
  echo "max open orders reached stopping script after delay"
  echo "you can change the limmit with: --tradesopen 5"
  echo "open orders: $OPEN_ORDERS"
  sleep $DELAY
  exit 0
fi
# get current pending orders to check if we should buy more
NONCE=$(nonce)
URI="https://bittrex.com/api/v1.1/market/getopenorders?apikey=${API_KEY}&nonce=${NONCE}"
SIGN=$(hash_hmac "sha512" $URI $API_SECRET | awk '{print $2}')
JSON=$(curl -s -H "apisign: $SIGN" "$URI")

TOTAL_OPEN_ORDERS=$(echo $JSON | jq -r '[.result[] |select (.Closed == null) .OrderUuid] | length')

if [ "$TOTAL_OPEN_ORDERS" -ge "$MAX_OPEN_TOTAL" ]; then
  echo "max total open orders reached stopping script after delay"
  echo "total open orders: $TOTAL_OPEN_ORDERS"
  sleep $DELAY
  exit 0
fi

# get market summaries
MARKET=$(curl -s "https://bittrex.com/api/v1.1/public/getmarketsummaries" | jq -r '.result')

# get current balance
NONCE=$(nonce)
URI="https://bittrex.com/api/v1.1/account/getbalances?apikey=${API_KEY}&nonce=${NONCE}"
SIGN=$(hash_hmac "sha512" $URI $API_SECRET | awk '{print $2}')
JSON=$(curl -s -H "apisign: $SIGN" "$URI")
FROM_AVAIL=$(echo $JSON | jq -r '.result[] |select (.Currency=="'$FROM'") .Available')
TO_AVAIL=$(echo $JSON | jq -r '.result[] |select (.Currency=="'$TO'") .Available')


# Get ticker
TICKER=$(curl -s "https://bittrex.com/api/v1.1/public/getticker?market=$PAIR" | jq -r '.result')
LAST=$(echo $TICKER | jq -r '.Last')
#BID=$(echo $TICKER | jq -r '.Bid')
#ASK=$(echo $TICKER | jq -r '.Ask')


# set invest amount
PERCENT_INV=$(python -c "print $PERCENT_INV/100.0")
INVEST_BTC=$(python -c "print $PERCENT_INV*$FROM_AVAIL")

# get high / low
LAST_200=$(curl -s "https://bittrex.com/api/v1.1/public/getmarkethistory?market=$PAIR&_=$(date +%s)" |jq -r '.result [] .Price')
PRICES_200=$(echo "$LAST_200" | sed ':a;N;$!ba;s/\n/, /g')
AVG_COMMAND="print sum([$PRICES_200])/200"
AVG_200=$(python -c "${AVG_COMMAND}")
AVG_200_MIN_05=$(python -c "print ($AVG_200 * 0.995)")
AVG_200_MIN_1=$(python -c "print ($AVG_200 * 0.99)")
LAST_HIGH=$(echo "$LAST_200" |sort -n |tail -n 1)
LAST_LOW=$(echo "$LAST_200" |sort -nr |tail -n 1)

DIFF_PERCENTAGE=$(python -c "print ((($LAST_HIGH / $LAST_LOW)-1)*100)")
DIFF_TO_LOW=$(python -c "if $DIFF_PERCENTAGE < $PERCENT_RET: print('true')")
if [ "$DIFF_CHECK" = "true" ]; then
  echo "checking high-low diff"
  echo "diff: $DIFF_PERCENTAGE%"
  if [ "$DIFF_TO_LOW" = "true"  ]; then
    echo "not enough diff between high and low"
    exit 0
  fi
fi
  


LAST_LOWER=$(python -c "if $LAST < $LAST_LOW: print('true')")
# if last is lower use that instead of low from last 200
if [ "$LAST_LOWER" = "true"  ]; then
  LOW=$LAST
else
  LOW=$LAST_LOW
fi

HIGH=$LAST_HIGH


# setup buy style
# 1   lowest price in range
# 2-9 10% steps in between the LOW and HIGH
# 10  highest price in range
case "$BUYSTYLE" in 
  1 ) 
  RATE=$LOW
  ;;
  [2-9] )
  RATE=$(python -c "print ($LOW+(($HIGH - $LOW)/10)*$BUYSTYLE)")
  ;;
  10 )
  RATE=$HIGH
  ;; 
esac

if [ "$MAX_BUY" = "AVG-05" ]; then
  MAX_BUY="$AVG_200_MIN_05"
fi 

OVER_LIMIT=$(python -c "if $RATE > $MAX_BUY: print 'true'")
if [ "$OVER_LIMIT" == "true" ] ; then
  echo "price above limit"
  echo "MAX: $MAX_BUY"
  echo "NOW: $RATE"
  exit 0
fi

# set sell price
PERCENT_RET=$(echo "$PERCENT_RET + 100" | bc)
INT_RATE=$(python -c "print $PERCENT_RET/100.0")
MIN_SELL=$(python -c "print $RATE*$INT_RATE")

# how much to buy
TO_QUANTITY="$(python -c "print $INVEST_BTC/$RATE")" 

# market info
PAIR_SUMMARY=$(echo $MARKET |  jq -r '.[] |select (.MarketName=="'$PAIR'")')
DAY_HIGH=$(echo $PAIR_SUMMARY | jq -r '.High') 

if [ "$DEBUG" = "YES"  ]; then
  echo "Pair:       $PAIR"
  echo "From:       $FROM"
  echo "To:         $TO" 
  echo ""
  echo "Balances:"
  echo "Invest:     $INVEST_BTC"
  echo "$FROM:        $FROM_AVAIL"
  echo   "$TO:        $TO_AVAIL"
  echo ""
  echo "200 Low:    $LAST_LOW"
  echo "Day High:   $DAY_HIGH"
  echo ""
  echo "LOW:        $LOW "
  echo "HIGH:       $HIGH"
  echo "--------------------------"
  echo "Buy:        $RATE" 
  echo "Sell:       $MIN_SELL"
  echo "--------------------------"
  read -p "Press enter to continue"
  echo ""
else
  echo ""
  echo "AVG 200:    $AVG_200"
  echo "AVG -1%     $AVG_200_MIN_1"
  echo "Buy:        $RATE" 
  echo "Sell:       $MIN_SELL"
  echo "Amount:     $TO_QUANTITY"
  echo ""
fi

# provide a decent kill switch
# trap ctrl-c and call ctrl_c()
trap ctrl_c INT
function ctrl_c() {
  echo "** Trapped CTRL-C"
  stop_order $UUID
  exit 1
}

buy $PAIR $TO_QUANTITY $RATE 
BUY_STATUS="$?"

if [ "$DEBUG" = "YES"  ]; then
  echo "status code: $BUY_STATUS"
  echo "Status:      $STATUS"
  echo "Message:     $MESSAGE"
  echo "UUID:        $UUID"
  read -p "Press enter to continue"
  echo ""
fi

if [ $BUY_STATUS -eq 0 ]; then
  # wait for buy to be filled
  waiting=1

  bigloop=0
  loop=0

  echo "waiting for buy to pass"
  spin='-\|/'
  spini=0
  
  while [ $waiting -eq 1 ]; do
    # prety wait spinning thing
    i=$(( (i+1) %4 ))
    printf "\r${spin:$i:1}"

    bigloop=$(echo "$bigloop + 1" | bc)
    if [ "$bigloop" -eq "$(echo "$LOOPS * 5"| bc)" ]; then
      echo "bigloop killed it"
      stop_order $UUID && exit 1
    fi
    
    check_order $UUID 
    SELL_STATUS=$?

    if [ $SELL_STATUS -eq 0 ]; then
      waiting=0
      # colors
      GREEN='\033[0;32m'
      NC='\033[0m' # No Color
      echo -e "\n ${GREEN}Placing sell at: $MIN_SELL $PAIR ${NC}\n"
      sell_order $PAIR $TO_QUANTITY $MIN_SELL

      # unset crtl_c trap
      function ctrl_c() {
        exit 1
      }

      # delay end of the script after a succesfull position has been taken
      sleep $DELAY
    else
      loop=$(echo "$loop + 1" | bc)
      if [ "$loop" = "$LOOPS" ]; then
        echo " max loops checking next move"
        if [ "$CREEP" = "true" ]; then
          echo "creeping in on price"
          # up the buy price
          RATE=$(python -c "print ($RATE + ($CREEP_NUM / 100000000.0))")
          OVER_LIMIT=$(python -c "if $RATE > $MAX_BUY: print 'true'")
          if [ "$OVER_LIMIT" == "true" ] ; then
            echo "price over limit"
            stop_order $UUID && exit 1
          else
            echo "New buy rate: $RATE"
            # stop pending order
            stop_order $UUID
            buy $PAIR $TO_QUANTITY $RATE
            BUY_STATUS="$?"
            if [ $BUY_STATUS -eq 0 ]; then
              loop=0
              echo "waiting for buy to pass"
            fi
          fi
        else
          echo "No creep enabled lets stope here"
          stop_order $UUID && exit 1
        fi
      fi
    fi

    sleep .2
  done
else 
  echo "The following error in buy order stopped the script:"
  echo "$MESSAGE"
fi
