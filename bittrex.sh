#!/bin/bash

source api_keys.sh

function hash_hmac {
  digest="$1"
  data="$2"
  key="$3"
  shift 3
  echo -n "$data" | openssl dgst "-$digest" -hmac "$key" "$@"
}

# nonce (random use once number)
NONCE=$(cat /dev/urandom | tr -dc '0-9' | fold -w 256 | head -n 1 | sed -e 's/^0*//' | head --bytes 16)


URI="https://bittrex.com/api/v1.1/account/getbalances?apikey=${API_KEY}&nonce=${NONCE}"
SIGN=$(hash_hmac "sha512" $URI $API_SECRET | awk '{print $2}')

JSON=$(curl -s -H "apisign: $SIGN" "$URI")

COINS=$(echo $JSON | jq -r '.result[] .Currency')

TOTAL_BTC=0

BASE="BTC"

for coin in $COINS ; do
  # get value and check worth in eur
  jcoin=$(echo "\"$coin\"")
#  echo $coin
  balance=$(echo $JSON | jq '.result[] | select(.Currency=='$jcoin') .Balance')
#  echo $balance
  if [ "$coin" != "$BASE" ] ; then
    rate=$(curl -s "https://bittrex.com/api/v1.1/public/getticker?market=BTC-$coin" |jq '.result .Last')
  else
    rate=1
  fi
#  echo $rate
  val=$(python -c "print $balance*$rate")
#  echo $val
  TOTAL_BTC=$(python -c "print $TOTAL_BTC+$val")
done

echo $TOTAL_BTC
