#!/bin/bash

MARKET=$(curl -s "https://api.coinmarketcap.com/v1/ticker/?limit=50" | jq -r '.[] .symbol')



for coin in $MARKET ; do
  echo $coin
### buy / sell order book
  coin_last_trades_json=$(curl -s "https://bittrex.com/api/v1.1/public/getmarkethistory?market=BTC-$coin")
  success=$(echo $coin_last_trades_json |jq '.success')
  if [ "$success" == "true" ] ; then
    BUY=$(echo $coin_last_trades_json|jq -r '.result [] | select(.OrderType=="BUY") .Id' |wc -l)
    SELL=$(echo $coin_last_trades_json|jq -r '.result [] | select(.OrderType=="SELL") .Id' |wc -l)
    echo "$coin: $BUY $SELL"
  fi
  #  ticker
  coin_ticker=$(curl -s "https://bittrex.com/api/v1.1/public/getticker?market=BTC-$coin")
  success=$(echo $coin_ticker |jq '.success')
  if [ "$success" == "true" ] ; then
    echo $coin_ticker
  fi
done

