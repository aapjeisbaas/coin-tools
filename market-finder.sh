#!/bin/bash

# params
PERCENT_RET="1"


# Get markets over 500 ase volume btc-* markets where value is over 0.00001 BTC
MARKETS=$(curl -s "https://bittrex.com/api/v1.1/public/getmarketsummaries" | jq -r '.result[] | select (.MarketName | contains ("BTC-")) | select (.BaseVolume > 300) | select (.Last > 0.00001)')


PAIRS=$(echo $MARKETS | jq -r '.MarketName')


for PAIR in $PAIRS ; do
  # get high / low
  LAST_200=$(curl -s "https://bittrex.com/api/v1.1/public/getmarkethistory?market=$PAIR&_=$(date +%s)" |jq -r '.result [] .Price')
  PRICES_200=$(echo "$LAST_200" | sed ':a;N;$!ba;s/\n/, /g')
  AVG_COMMAND="print sum([$PRICES_200])/200"
  AVG_200=$(python -c "${AVG_COMMAND}")
  AVG_200_MIN_05=$(python -c "print ($AVG_200 * 0.995)")
  AVG_200_MIN_1=$(python -c "print ($AVG_200 * 0.99)")
  LAST_HIGH=$(echo "$LAST_200" |sort -n |tail -n 1)
  LAST_LOW=$(echo "$LAST_200" |sort -nr |tail -n 1)


  
  DIFF_PERCENTAGE=$(python -c "print ((($LAST_HIGH / $LAST_LOW)-1)*100)")
  DIFF_TO_LOW=$(python -c "if $DIFF_PERCENTAGE < $PERCENT_RET: print('true')")
  if [ "$DIFF_TO_LOW" != "true"  ]; then
    VOLUME=$(echo "$MARKETS" | jq -r 'select(.MarketName=="'$PAIR'") .BaseVolume' )
    PREV_DAY=$(echo "$MARKETS" | jq -r 'select(.MarketName=="'$PAIR'") .PrevDay' )
    DAY_DIFF=$(python -c "print ((( $LAST_LOW / $PREV_DAY )-1)*100)")
    DAY_DIFF_TO_LOW=$(python -c "if $DAY_DIFF < $PERCENT_RET: print('true')")
    if [ "$DAY_DIFF_TO_LOW" != "true" ]; then
      TICKERS=$(curl -s 'https://bittrex.com/Api/v2.0/pub/market/GetTicks?marketName=BTC-NEO&tickInterval=OneMin')
      ONE_HOUR_BACK=$(echo $TICKERS | jq '.result[] | select (.T | contains("'$(date -u -d @$(echo "$(date -u +%s) - 3600" |bc) +%FT%H:%M)'"))' | jq -r '.C')
      HALF_HOUR_BACK=$(echo $TICKERS | jq '.result[] | select (.T | contains("'$(date -u -d @$(echo "$(date -u +%s) - 1800" |bc) +%FT%H:%M)'"))' | jq -r '.C')

      HOUR_DIFF=$(python -c "print ((( $LAST_LOW / $ONE_HOUR_BACK )-1)*100)") # get hour percentage

      HOUR_DIFF_TO_LOW=$(python -c "if $ONE_HOUR_BACK > $AVG_200: print('true')")
      if [ "$HOUR_DIFF_TO_LOW" != "true" ]; then
        echo "$PAIR $VOLUME $DIFF_PERCENTAGE $DAY_DIFF"
      fi
    fi
  fi
done | sort -n -k 3 -r

