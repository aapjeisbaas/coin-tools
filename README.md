![Sorry but I like to keep track of my projects](https://perzik.aapjeisbaas.nl/perzik.perzik?idsite=4&rec=1&action_name=coin-tools)
# DYOR! #

These scripts tools and tests are mostly based around coinmarketcap and bittrex api's
Use at your own risk, this project is under development and things will change loads.

## Usage ##
Bittrex api keys, place them in: `api_keys.sh`
```
API_KEY="*******"
API_SECRET="*****"
```

Example usage of take-position.sh:
```
# normal
./take-position.sh --roi 1 --investpercent 2 --buystyle 2 --pair BTC-OMG -m 0.00198

Buy:        0.001962278
Sell:       0.00199171217

\waiting for buy to pass

Placing sell at: 0.00199171217


# debug
./take-position.sh --roi 1 --investpercent 2 --buystyle 2 --pair BTC-OMG -m 0.00198 --debug
Pair:       BTC-OMG
From:       BTC
To:         OMG

Balances:
Invest:     0.0005041646
BTC:        0.02520823
OMG:        0

200 Low:    0.00195935
Day High:   0.00222545

LOW:        0.00195935
HIGH:       0.00197399
--------------------------
Buy:        0.001962278
Sell:       0.00199171217
--------------------------
Press enter to continue

status code: 0
Status:      true
Message:
UUID:        921d75cd-3ccc-4711-9053-2c7f2b63c37d
Press enter to continue

waiting for buy to pass
\cheching UUID: 921d75cd-3ccc-4711-9053-2c7f2b63c37d
Status:  true
Message:
Closed:
|cheching UUID: 921d75cd-3ccc-4711-9053-2c7f2b63c37d
Status:  true
Message:
Closed:  {
  "AccountId": null,
  "OrderUuid": "921d75cd-3ccc-4711-9053-2c7f2b63c37d",
  "Exchange": "BTC-OMG",
  "Type": "LIMIT_BUY",
  "Quantity": 0.25692822,
  "QuantityRemaining": 0,
  "Limit": 0.00196227,
  "Reserved": 0.00050416,
  "ReserveRemaining": 0.00050416,
  "CommissionReserved": 1.26e-06,
  "CommissionReserveRemaining": 0,
  "CommissionPaid": 1.26e-06,
  "Price": 0.00050416,
  "PricePerUnit": 0.00196226,
  "Opened": "2017-08-17T22:52:27.69",
  "Closed": "2017-08-17T22:52:28.953",
  "IsOpen": false,
  "Sentinel": "53ce027d-f301-4c5d-bc92-733e65c59531",
  "CancelInitiated": false,
  "ImmediateOrCancel": false,
  "IsConditional": false,
  "Condition": "NONE",
  "ConditionTarget": null
}
Closed the buy order.

Placing sell at: 0.00199171217
{
  "success": true,
  "message": "",
  "result": {
    "uuid": "895d831b-f6d8-4e00-a262-4c60c7f3043c"
  }
}
```

